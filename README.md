<pre>
    ____   ____   __  __ ______ ____ __    _____
   / __ \ / __ \ / / / //_  __//  _// /   / ___/
  / /_/ // /_/ // / / /  / /   / / / /    \__ \ 
 / ____// ____// /_/ /  / /  _/ / / /___ ___/ / 
/_/    /_/     \____/  /_/  /___//_____//____/  
</pre>

Welcome to Pat Prodanovic's utilities for free surface flow modeling!

PPUTILS consist of a number of scripts that assist the user in completing environmental modeling projects using nothing but free and open source tools. Tasks that PPUTILS excels at are: development of digital surface models through creation of Triangular Irregular Networks (TINs), development of quality meshes and model grids for use in 2D and 3D numerical flow and wave modeling, and pre- and post-processing of model output for use in open source Geographic Information Systems (GIS) and other model viewers.

The PPUTILS project was originally designed to be used with the open source TELEMAC modeling system, but works well with SWAN and SWASH numerical models too. TELEMAC is a 2D/3D finite element environmental modeling system used in the study river and coastal hydraulics, wave generation and transformation, sediment transport, water quality, and other studies. SWAN and SWASH are numerical wave models used to generation and propagation of waves in coastal environments.

The scripts in the PPUTILS project follow the Unix/Linux convention where each tool is designed to do one thing well. There is no graphical user interface (GUI), as PPUTILS inputs and outputs can be visualized in open source GIS applications.

For more detailed information on the tools, please consult the user's manual. 
